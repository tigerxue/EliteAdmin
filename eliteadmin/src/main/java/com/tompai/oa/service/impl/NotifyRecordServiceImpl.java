/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.oa.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tompai.oa.dao.NotifyRecordDao;
import com.tompai.oa.domain.NotifyRecordDO;
import com.tompai.oa.service.NotifyRecordService;

@Service
public class NotifyRecordServiceImpl implements NotifyRecordService {
	@Autowired
	private NotifyRecordDao notifyRecordDao;

	@Override
	public NotifyRecordDO get(Long id) {
		return notifyRecordDao.get(id);
	}

	@Override
	public List<NotifyRecordDO> list(Map<String, Object> map) {
		return notifyRecordDao.list(map);
	}

	@Override
	public int count(Map<String, Object> map) {
		return notifyRecordDao.count(map);
	}

	@Override
	public int save(NotifyRecordDO notifyRecord) {
		return notifyRecordDao.save(notifyRecord);
	}

	@Override
	public int update(NotifyRecordDO notifyRecord) {
		return notifyRecordDao.update(notifyRecord);
	}

	@Override
	public int remove(Long id) {
		return notifyRecordDao.remove(id);
	}

	@Override
	public int batchRemove(Long[] ids) {
		return notifyRecordDao.batchRemove(ids);
	}

	@Override
	public int changeRead(NotifyRecordDO notifyRecord) {
		return notifyRecordDao.changeRead(notifyRecord);
	}

}
