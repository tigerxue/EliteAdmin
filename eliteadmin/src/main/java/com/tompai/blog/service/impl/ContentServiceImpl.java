/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.blog.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tompai.blog.dao.ContentDao;
import com.tompai.blog.domain.ContentDO;
import com.tompai.blog.service.ContentService;

/**
 * @author: tompai
 * @email：liinux@qq.com
 * @createTime: 2019年11月24日 下午11:55:29
 * @history:
 * @version: v1.0
 */
@Service
public class ContentServiceImpl implements ContentService {
	@Autowired
	private ContentDao bContentMapper;

	@Override
	public ContentDO get(Long cid) {
		return bContentMapper.get(cid);
	}

	@Override
	public List<ContentDO> list(Map<String, Object> map) {
		return bContentMapper.list(map);
	}

	@Override
	public int count(Map<String, Object> map) {
		return bContentMapper.count(map);
	}

	@Override
	public int save(ContentDO bContent) {
		return bContentMapper.save(bContent);
	}

	@Override
	public int update(ContentDO bContent) {
		return bContentMapper.update(bContent);
	}

	@Override
	public int remove(Long cid) {
		return bContentMapper.remove(cid);
	}

	@Override
	public int batchRemove(Long[] cids) {
		return bContentMapper.batchRemove(cids);
	}

}
