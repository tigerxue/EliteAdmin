/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.common.service;

import org.springframework.stereotype.Service;

import com.tompai.common.domain.LogDO;
import com.tompai.common.domain.PageDO;
import com.tompai.common.utils.Query;

@Service
public interface LogService {
	void save(LogDO logDO);

	PageDO<LogDO> queryList(Query query);

	int remove(Long id);

	int batchRemove(Long[] ids);
}
