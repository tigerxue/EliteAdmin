/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.common.utils;

import javax.servlet.http.HttpServletRequest;

public class HttpServletUtils {
	public static boolean jsAjax(HttpServletRequest req) {
		// 判断是否为ajax请求，默认不是
		boolean isAjaxRequest = false;
		if (!StringUtils.isBlank(req.getHeader("x-requested-with"))
				&& req.getHeader("x-requested-with").equals("XMLHttpRequest")) {
			isAjaxRequest = true;
		}
		return isAjaxRequest;
	}
}
