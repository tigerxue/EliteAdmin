/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.common.utils;

import java.util.HashMap;
import java.util.Map;

public class MsgResult extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;

	public MsgResult() {
		put("code", 0);
		put("msg", "操作成功");
	}

	public static MsgResult error() {
		return error(1, "操作失败");
	}

	public static MsgResult error(String msg) {
		return error(500, msg);
	}

	public static MsgResult error(int code, String msg) {
		MsgResult msgResult = new MsgResult();
		msgResult.put("code", code);
		msgResult.put("msg", msg);
		return msgResult;
	}

	public static MsgResult ok(String msg) {
		MsgResult msgResult = new MsgResult();
		msgResult.put("msg", msg);
		return msgResult;
	}

	public static MsgResult ok(Map<String, Object> map) {
		MsgResult msgResult = new MsgResult();
		msgResult.putAll(map);
		return msgResult;
	}

	public static MsgResult ok() {
		return new MsgResult();
	}

	@Override
	public MsgResult put(String key, Object value) {
		super.put(key, value);
		return this;
	}
}
