
/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.common.utils;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * @desc: snow
 * @name: MapTest.java
 * @author: tompai liinux@qq.com
 * @createTime: 2019年11月24日 下午10:13:40
 * @history:
 * @version: v1.0
 */

public class MapTest {

	/**
	 * @author: tompai
	 * @createTime: 2019年11月24日 下午10:13:40
	 * @history:
	 * @param args void
	 */

	public static void main(String[] args) {
		int initialCapacity = 16;
		int aHundredMillion = 10000000;
		Map<String, String> hm = new HashMap<String, String>(initialCapacity);

		for (int i = 0; i < aHundredMillion; i++) {
			String sts = RandomStringUtils.random(6, true, true);
			hm.put(String.valueOf(i), sts);
		}

		// 1. keySet
		Instant start = Instant.now();
		for (String key : hm.keySet()) {
			// System.out.println(key + ": " + hm.get(key));
		}
		Instant end = Instant.now();
		System.out.println("KeySet Time use:" + Duration.between(start, end).toMillis());

		// 2. entrySet
		start = Instant.now();
		for (Entry<String, String> entry : hm.entrySet()) {
			// System.out.println(entry.getKey() + ": " + entry.getValue());
		}
		end = Instant.now();
		System.out.println("EntrySet Time use:" + Duration.between(start, end).toMillis());

		// 3. forEach
		start = Instant.now();
		hm.forEach((key, value) -> {
			// System.out.println(key + ": " + value);
		});
		end = Instant.now();
		System.out.println("Map forEach Time use:" + Duration.between(start, end).toMillis());
	}

}
