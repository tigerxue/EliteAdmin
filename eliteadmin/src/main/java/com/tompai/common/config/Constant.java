/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.common.config;

public class Constant {
	// 演示系统账户
	public static String DEMO_ACCOUNT = "test";
	// 自动去除表前缀
	public static String AUTO_REOMVE_PRE = "true";
	// 停止计划任务
	public static String STATUS_RUNNING_STOP = "stop";
	// 开启计划任务
	public static String STATUS_RUNNING_START = "start";
	// 通知公告阅读状态-未读
	public static String OA_NOTIFY_READ_NO = "0";
	// 通知公告阅读状态-已读
	public static int OA_NOTIFY_READ_YES = 1;
	// 部门根节点id
	public static Long DEPT_ROOT_ID = 0l;
	// 缓存方式
	public static String CACHE_TYPE_REDIS = "redis";

	public static String LOG_ERROR = "error";

}
