/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfigurer implements WebMvcConfigurer {

	@Autowired
	TomPaiConfig tomPaiConfig;

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/index").setViewName("index");
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/files/**").addResourceLocations("file:///" + tomPaiConfig.getUploadPath());
	}

	/** 解决跨域问题 **/
	// public void addCorsMappings(CorsRegistry registry) ;
	/** 添加拦截器 **/
	// void addInterceptors(InterceptorRegistry registry);
	/** 这里配置视图解析器 **/
	// void configureViewResolvers(ViewResolverRegistry registry);
	/** 配置内容裁决的一些选项 **/
	// void configureContentNegotiation(ContentNegotiationConfigurer configurer);
	/** 视图跳转控制器 **/
	// void addViewControllers(ViewControllerRegistry registry);
	/** 静态资源处理 **/
	// void addResourceHandlers(ResourceHandlerRegistry registry);
	/** 默认静态资源处理器 **/
	// void configureDefaultServletHandling(DefaultServletHandlerConfigurer
	// configurer);

}
