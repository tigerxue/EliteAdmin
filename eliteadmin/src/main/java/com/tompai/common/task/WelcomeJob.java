/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.common.task;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.tompai.oa.domain.Response;

@Component
public class WelcomeJob implements Job {
	@Autowired
	SimpMessagingTemplate template;

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		template.convertAndSend("/topic/getResponse", new Response(
				"欢迎体验tompai,这是一个任务计划，使用了websocket和quzrtz技术，可以在计划列表中取消，欢迎您加入【https://gitee.com/tigerxue/EliteAdmin】交流学习!"));

	}

}