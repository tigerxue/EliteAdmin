/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.activiti.vo;

import org.activiti.engine.task.Task;

/**
 * @author: tompai
 * @email：liinux@qq.com
 * @createTime: 2019年11月24日 下午11:55:29
 * @history:
 * @version: v1.0
 */
public class TaskVO {

	public TaskVO(Task task) {

		this.setId(task.getId());
		this.setKey(task.getTaskDefinitionKey());
		this.setName(task.getName());
		this.setDescription(task.getDescription());
		this.setAssignee(task.getAssignee());
		this.setFormKey(task.getFormKey());
		this.setProcessId(task.getProcessInstanceId());
		this.setProcessDefinitionId(task.getProcessDefinitionId());
		this.setExecutionId(task.getExecutionId());
	}

	private String id;
	private String name;
	private String key;
	private String description;
	private String formKey;
	private String assignee;
	private String processId;
	private String processDefinitionId;
	private String executionId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFormKey() {
		return formKey;
	}

	public void setFormKey(String formKey) {
		this.formKey = formKey;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getExecutionId() {
		return executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
}
