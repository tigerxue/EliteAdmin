/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.activiti.service;

import java.io.InputStream;

import org.activiti.engine.repository.Model;
import org.springframework.stereotype.Service;

/**
 * @author: tompai
 * @email：liinux@qq.com
 * @createTime: 2019年11月24日 下午11:55:29
 * @history:
 * @version: v1.0
 */
@Service
public interface ProcessService {
	Model convertToModel(String procDefId) throws Exception;

	InputStream resourceRead(String id, String resType) throws Exception;
}
