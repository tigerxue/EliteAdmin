/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.activiti.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tompai.activiti.config.ActivitiConstant;
import com.tompai.activiti.dao.SalaryDao;
import com.tompai.activiti.domain.SalaryDO;
import com.tompai.activiti.service.SalaryService;

/**
 * @author: tompai
 * @email：liinux@qq.com
 * @createTime: 2019年11月24日 下午11:55:29
 * @history:
 * @version: v1.0
 */
@Service
public class SalaryServiceImpl implements SalaryService {
	@Autowired
	private SalaryDao salaryDao;
	@Autowired
	private ActTaskServiceImpl actTaskService;

	@Override
	public SalaryDO get(String id) {
		return salaryDao.get(id);
	}

	@Override
	public List<SalaryDO> list(Map<String, Object> map) {
		return salaryDao.list(map);
	}

	@Override
	public int count(Map<String, Object> map) {
		return salaryDao.count(map);
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public int save(SalaryDO salary) {
		salary.setId(UUID.randomUUID().toString().replace("-", ""));
		actTaskService.startProcess(ActivitiConstant.ACTIVITI_SALARY[0], ActivitiConstant.ACTIVITI_SALARY[1],
				salary.getId(), salary.getContent(), new HashMap<>());
		return salaryDao.save(salary);
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public int update(SalaryDO salary) {
		Map<String, Object> vars = new HashMap<>(16);
		vars.put("pass", salary.getTaskPass());
		vars.put("title", "");
		actTaskService.complete(salary.getTaskId(), vars);
		return salaryDao.update(salary);
	}

	@Override
	public int remove(String id) {
		return salaryDao.remove(id);
	}

	@Override
	public int batchRemove(String[] ids) {
		return salaryDao.batchRemove(ids);
	}

}
