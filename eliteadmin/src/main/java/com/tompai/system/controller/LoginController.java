/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.system.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tompai.common.annotation.Log;
import com.tompai.common.config.TomPaiConfig;
import com.tompai.common.controller.BaseController;
import com.tompai.common.domain.FileDO;
import com.tompai.common.domain.Tree;
import com.tompai.common.service.FileService;
import com.tompai.common.utils.MD5Utils;
import com.tompai.common.utils.MsgResult;
import com.tompai.common.utils.ShiroUtils;
import com.tompai.common.utils.StringUtils;
import com.tompai.common.utils.ValidateCodeUtil;
import com.tompai.system.domain.MenuDO;
import com.tompai.system.service.MenuService;

@Controller
public class LoginController extends BaseController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	MenuService menuService;
	@Autowired
	FileService fileService;
	@Autowired
	TomPaiConfig tomPaiConfig;

	@GetMapping({ "/", "" })
	String welcome(Model model) {

		return "redirect:/blog";
	}

	@Log("请求访问主页")
	@GetMapping({ "/index" })
	String index(Model model) {
		List<Tree<MenuDO>> menus = menuService.listMenuTree(getUserId());
		model.addAttribute("menus", menus);
		model.addAttribute("name", getUser().getName());
		FileDO fileDO = fileService.get(getUser().getPicId());
		if (fileDO != null && fileDO.getUrl() != null) {
			if (fileService.isExist(fileDO.getUrl())) {
				model.addAttribute("picUrl", fileDO.getUrl());
			} else {
				model.addAttribute("picUrl", "/img/photo_s.jpg");
			}
		} else {
			model.addAttribute("picUrl", "/img/photo_s.jpg");
		}
		model.addAttribute("username", getUser().getUsername());
		return "index";
	}

	@GetMapping("/login")
	String login(Model model) {
		model.addAttribute("username", tomPaiConfig.getUsername());
		model.addAttribute("password", tomPaiConfig.getPassword());
		return "login";
	}

	@Log("登录")
	@PostMapping("/login")
	@ResponseBody
	MsgResult ajaxLogin(String username, String password, String verify, HttpServletRequest request) {

		try {
			// 从session中获取随机数
			String random = (String) request.getSession().getAttribute(ValidateCodeUtil.RANDOMCODEKEY);
			if (StringUtils.isBlank(verify)) {
				return MsgResult.error("请输入验证码");
			}
			if (!random.equals(verify)) {
				return MsgResult.error("请输入正确的验证码");
			}
		} catch (Exception e) {
			logger.error("验证码校验失败", e);
			return MsgResult.error("验证码校验失败");
		}
		password = MD5Utils.encrypt(username, password);
		UsernamePasswordToken token = new UsernamePasswordToken(username, password);
		Subject subject = SecurityUtils.getSubject();
		try {
			subject.login(token);
			return MsgResult.ok();
		} catch (AuthenticationException e) {
			return MsgResult.error("用户或密码错误");
		}
	}

	@GetMapping("/logout")
	String logout() {
		ShiroUtils.logout();
		return "redirect:/login";
	}

	@GetMapping("/main")
	String main() {
		return "main";
	}

	/**
	 * 生成验证码
	 */
	@GetMapping(value = "/getVerify")
	public void getVerify(HttpServletRequest request, HttpServletResponse response) {
		try {
			response.setContentType("image/jpeg");// 设置相应类型,告诉浏览器输出的内容为图片
			response.setHeader("Pragma", "No-cache");// 设置响应头信息，告诉浏览器不要缓存此内容
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expire", 0);
			ValidateCodeUtil randomValidateCode = new ValidateCodeUtil();
			randomValidateCode.getRandcode(request, response);// 输出验证码图片方法
		} catch (Exception e) {
			logger.error("获取验证码失败>>>> ", e);
		}
	}

}
