/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tompai.common.domain.Tree;
import com.tompai.common.utils.BuildTree;
import com.tompai.system.dao.DeptDao;
import com.tompai.system.domain.DeptDO;
import com.tompai.system.service.DeptService;

@Service
public class DeptServiceImpl implements DeptService {
	@Autowired
	private DeptDao sysDeptMapper;

	@Override
	public DeptDO get(Long deptId) {
		return sysDeptMapper.get(deptId);
	}

	@Override
	public List<DeptDO> list(Map<String, Object> map) {
		return sysDeptMapper.list(map);
	}

	@Override
	public int count(Map<String, Object> map) {
		return sysDeptMapper.count(map);
	}

	@Override
	public int save(DeptDO sysDept) {
		return sysDeptMapper.save(sysDept);
	}

	@Override
	public int update(DeptDO sysDept) {
		return sysDeptMapper.update(sysDept);
	}

	@Override
	public int remove(Long deptId) {
		return sysDeptMapper.remove(deptId);
	}

	@Override
	public int batchRemove(Long[] deptIds) {
		return sysDeptMapper.batchRemove(deptIds);
	}

	@Override
	public Tree<DeptDO> getTree() {
		List<Tree<DeptDO>> trees = new ArrayList<Tree<DeptDO>>();
		List<DeptDO> sysDepts = sysDeptMapper.list(new HashMap<String, Object>(16));
		for (DeptDO sysDept : sysDepts) {
			Tree<DeptDO> tree = new Tree<DeptDO>();
			tree.setId(sysDept.getDeptId().toString());
			tree.setParentId(sysDept.getParentId().toString());
			tree.setText(sysDept.getName());
			Map<String, Object> state = new HashMap<>(16);
			state.put("opened", true);
			tree.setState(state);
			trees.add(tree);
		}
		// 默认顶级菜单为０，根据数据库实际情况调整
		Tree<DeptDO> t = BuildTree.build(trees);
		return t;
	}

	@Override
	public boolean checkDeptHasUser(Long deptId) {
		// TODO Auto-generated method stub
		// 查询部门以及此部门的下级部门
		int result = sysDeptMapper.getDeptUserNumber(deptId);
		return result == 0 ? true : false;
	}

	@Override
	public List<Long> listChildrenIds(Long parentId) {
		List<DeptDO> deptDOS = list(null);
		return treeMenuList(deptDOS, parentId);
	}

	List<Long> treeMenuList(List<DeptDO> menuList, long pid) {
		List<Long> childIds = new ArrayList<>();
		for (DeptDO mu : menuList) {
			// 遍历出父id等于参数的id，add进子节点集合
			if (mu.getParentId() == pid) {
				// 递归遍历下一级
				treeMenuList(menuList, mu.getDeptId());
				childIds.add(mu.getDeptId());
			}
		}
		return childIds;
	}

}
